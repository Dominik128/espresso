package ostfalia.martens.espresso_example

import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.kotlin_fragment.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.browse

class KotlinFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.kotlin_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button.onClick {
            val userURL = url.text.toString().toURL ?: run {
                tilURL.error = "No valid URL entered"
                return@onClick
            }

            browse(userURL).also {
                tilURL.error = null
            }
        }
    }

    private val String.toURL: String?
        get() = if (Patterns.WEB_URL.matcher(this).matches()) this else null
}