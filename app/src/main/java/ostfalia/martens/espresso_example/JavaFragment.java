package ostfalia.martens.espresso_example;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class JavaFragment extends Fragment {

    private Button changeButton;
    private TextView textView;
    private EditText editText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.java_fragment, container, false);
        changeButton = view.findViewById(R.id.button);
        textView = view.findViewById(R.id.textView);
        editText = view.findViewById(R.id.editText);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String toBeChanged = editText.getText().toString();
                textView.setText(toBeChanged);
                editText.getText().clear();
            }
        });
    }
}
