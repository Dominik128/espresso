package ostfalia.martens.espresso_example

import android.content.Context
import android.content.Intent
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiSelector
import androidx.test.uiautomator.Until
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class UIAutomator {

    companion object {
        private const val packageName = "ostfalia.martens.espresso_example"
        private const val LAUNCH_TIMEOUT = 5000L

        private const val browserPackageName = "com.android.chrome"
    }

    private lateinit var device: UiDevice

    @Before
    fun startMainActivityFromHomeScreen() {
        // Initialize UiDevice instance
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

        // Start from the home screen
        device.pressHome()

        // Wait for launcher
        val launcherPackage = device.launcherPackageName
        assertNotNull(launcherPackage)
        device.wait(
                Until.hasObject(By.pkg(launcherPackage).depth(0)),
                LAUNCH_TIMEOUT
        )

        // Launch the app
        val context = ApplicationProvider.getApplicationContext<Context>()
        val intent = context.packageManager.getLaunchIntentForPackage(
                packageName)!!.apply {
            // Clear out any previous instances
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
        context.startActivity(intent)

        // Wait for the app to appear
        device.wait(
                Until.hasObject(By.pkg(packageName).depth(0)),
                LAUNCH_TIMEOUT
        )
    }

    @Test
    fun testOpenBrowser() {

        //switch to other fragment, we have to use the swipe gesture and click the localized string, because the navigation view is not supported...
        device.swipe(10, 100, 60, 100, 1)
        device.findObject(UiSelector().packageName(packageName).text("Kotlin Fragment")).click()


        //enter invalid url
        device.findObject(By.res(packageName, "url")).text = "https://ostfalia"
        device.findObject(By.res(packageName, "button")).click()

        //error should be visible and the browser should not open
        assertEquals(packageName, device.currentPackageName)

        device.findObject(By.res(packageName, "url")).text = "https://www.ostfalia.de"
        device.findObject(By.res(packageName, "button")).click()
        device.wait(
                Until.findObject(By.pkg(browserPackageName)), LAUNCH_TIMEOUT
        )
        assertEquals(browserPackageName, device.currentPackageName)
    }

}