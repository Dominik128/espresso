package ostfalia.martens.espresso_example;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class espresso {

    private String toBeTyped;

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setup() {
        toBeTyped = "Dies ist ein Test!";
    }

    @Test
    public void testUI() {
        // type Text in EditText
        onView(withId(R.id.editText)).perform(typeText(toBeTyped));

        // click Button
        onView(withId(R.id.button)).perform(click());

        // check TextView change
        onView(withId(R.id.textView)).check(matches(withText(toBeTyped)));

        // check if EditText is empty
        onView(withId(R.id.editText)).check(matches(withText("")));
    }
}
